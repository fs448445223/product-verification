package org.firas.common.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;

/**
 * <b><code></code></b>
 * <p/>
 * 与BigDecimal有关的工具类
 * <p/>
 *
 * <b>Creation Time:</b> 2018年08月23日
 *
 * @author Wu Yuping
 * @version 1.0.0
 * @since 1.0.0
 */
public final class BigDecimalUtils {

    private BigDecimalUtils() {}

    /**
     * 返回一个非null的BigDecimal
     * 主要用于将用JdbcTemplate查出来的List<Object[]>中的数字类型的值转换为BigDecimal
     * @param bigDecimal null或BigDecimal类型的值
     * @return 若bigDecimal为null，则返回BigDecimal.ZERO，否则返回bigDecimal
     */
    public static BigDecimal defaultBigDecimal(Object bigDecimal) {
        return null == bigDecimal ? BigDecimal.ZERO : BigDecimal.class.cast(bigDecimal);
    }

    /**
     * 将Number类型的值或字符串转换为BigDecimal
     * @param num Number类型的值或数字字符串
     * @return BigDecimal类型的值
     */
    public static BigDecimal toBigDecimal(Object num) {
        if (null == num) {
            return null;
        }
        if (num instanceof Number) {
            return numberToBigDecimal(Number.class.cast(num));
        }
        if (num instanceof CharSequence) {
            return new BigDecimal(num.toString());
        }
        throw new ClassCastException("不能将该对象转换为BigDecimal");
    }

    /**
     * 用于Number类型的值转换为BigDecimal
     * @param num Number类型的值
     * @return BigDecimal类型的值
     */
    public static BigDecimal numberToBigDecimal(Number num) {
        if (null == num) {
            return null;
        }
        if (num instanceof BigDecimal) {
            return BigDecimal.class.cast(num);
        }
        if (num instanceof Integer || num instanceof Short || num instanceof Byte) {
            return new BigDecimal(num.intValue());
        }
        if (num instanceof BigInteger) {
            return new BigDecimal(BigInteger.class.cast(num));
        }
        if (num instanceof Long) {
            return new BigDecimal(num.longValue());
        }
        return new BigDecimal(num.doubleValue());
    }

    /**
     * 用于Number类型的map[key]转换为BigDecimal
     * @param map Map
     * @param key map中的key
     * @return BigDecimal类型的值
     */
    public static BigDecimal numberToBigDecimal(Map<String, Object> map, String key) {
        return numberToBigDecimal(Number.class.cast(map.get(key)));
    }

    /**
     * 判断两个BigDecimal是否相等
     * @param a 第一个BigDecimal
     * @param b 第二个BigDecimal
     * @return a和b是否相等
     */
    public static boolean equals(BigDecimal a, BigDecimal b) {
        if (a == b) {
            return true;
        }
        if (null == a || null == b) {
            return false;
        }
        if (a.scale() <= 6 && b.scale() <= 6) {
            return a.compareTo(b) == 0;
        }
        return a.subtract(b).abs().compareTo(ERROR) <= 0;
    }

    private static final BigDecimal ERROR = new BigDecimal("0.0000001");

    public static boolean isZero(BigDecimal a) {
        return equals(BigDecimal.ZERO, a);
    }

    public static boolean isPositive(BigDecimal a) {
        return a.signum() > 0;
    }

    public static boolean isNegative(BigDecimal a) {
        return a.signum() < 0;
    }
}
