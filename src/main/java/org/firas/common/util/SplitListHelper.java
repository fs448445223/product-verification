package org.firas.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * <b><code></code></b>
 * <p/>
 * 根据ID批量查询数据库时，如果ID的数量过多，
 * 就需要分批来批量查询数据库。
 * 本工具类可将一个大的Collection的Iterator变为
 * 最多只有partitionSize个元素的Iterator的Iterator
 * <p/>
 * 例如idSet是一个有超过1000个ID的Set，但数据库只允许一次
 * 查询1000个id，则可以这样来查询idSet中的ID对应的所有元素：
 * <code>
 * Map&lt;String, BO&gt; result = new HashMap&lt;&gt;();
 * SplitListHelper&lt;String$gt; helper = new SplitListHelper(
 *         idSet, 1000);
 * while (helper.hasNext()) {
 *     Iterator&lt;String&gt; iterator = helper.next();
 *     StringBuilder sqlBuilder = new StringBuilder("SELECT * FROM bo WHERE id IN (");
 *     while (iterator.hasNext()) {
 *         sqlBuilder.append("'" + iterator.next() + "',"); // 构造 IN 语句
 *     } // IN 语句中最多只会有 1000 个元素
 *     sqlBuilder.setCharAt(sqlBuilder.length() - 1, ')'); // 将最后一个逗号改为右括号
 *     List&lt;BO&gt; list = boManager.searchBySql(sqlBuilder.toString());
 *     for (BO bo : list) {
 *         result.put(bo.getId(), bo);
 *     }
 * }
 * </code>
 * <p/>
 *
 * <b>Creation Time:</b> 2018年08月28日
 *
 * @author Wu Yuping
 * @version 1.0.0
 * @since 1.0.0
 */
public class SplitListHelper<T> implements Iterator<Iterator<T>> {

    private int size, partitionSize, current;
    private Iterator<T> realIterator;

    public SplitListHelper(Collection<T> list, int partitionSize) {
        if (partitionSize < 1) {
            throw new IllegalArgumentException("\"partitionSize\" must be at least 1");
        }
        this.partitionSize = partitionSize;
        size = list.size();
        current = 0;
        realIterator = list.iterator();
    }

    @Override public boolean hasNext() {
        if (current == size) {
            return false;
        }
        if (current % partitionSize == 0) {
            return true;
        }
        throw new IllegalStateException();
    }

    @Override public Iterator<T> next() {
        return new MyIterator();
    }

    private class MyIterator implements Iterator<T> {

        private final int max = Math.min(current + partitionSize, size);

        @Override public boolean hasNext() {
            return current < max;
        }

        @Override public T next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            current += 1;
            return realIterator.next();
        }

        @Override public void remove() {
            throw new RuntimeException("Operation not supported");
        }
    }

    @Override public void remove() {
        throw new RuntimeException("Operation not supported");
    }

    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>(10);
        for (int i = 0; i < 10; i += 1) {
            list.add(i);
        }
        testOutput(new SplitListHelper<>(list, 2));
        System.out.println("\n");
        testOutput(new SplitListHelper<>(list, 3));
        System.out.println("\n");
        testOutput(new SplitListHelper<>(list, 4));
    }

    private static <T> void testOutput(SplitListHelper<T> helper) {
        while (helper.hasNext()) {
            Iterator<T> iterator = helper.next();
            while (iterator.hasNext()) {
                System.out.print(iterator.next());
                System.out.print(", ");
            }
            System.out.println();
        }
    }
}
