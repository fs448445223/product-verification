package org.firas.common.util;

/**
 * <b><code></code></b>
 * <p/>
 * 处理Oracle和Postgre数据库把空字符串存储成null的问题
 * 增加处理可能含有中文的字符串入库时，可能需要截短字符串的情况
 * <p/>
 *
 * <b>Creation Time:</b> 2018年09月18日
 *
 * @author Wu Yuping
 * @version 1.0.0
 * @since 1.0.0
 */
public final class DbStringUtils {

    private DbStringUtils() {}

    public static String nullIfEmpty(String str) {
        return null == str || str.isEmpty() ? null : str;
    }

    public static String emptyIfNull(String str) {
        // return StringUtils.defaultString(str);
        return null == str ? "" : str;
    }

    public static boolean stringEquals(String a, String b) {
        return emptyIfNull(a).equals(emptyIfNull(b));
    }

    public static String ensureLength(String str, int length) {
        if (null == str) {
            return null;
        }
        int bytes = 0;
        for (int i = 0; i < str.length(); i += 1) {
            int code = str.codePointAt(i);
            bytes += code <= Byte.MAX_VALUE ? 1 : 2;
            if (bytes > length) {
                return str.substring(0, i);
            }
        }
        return str;
    }
}
