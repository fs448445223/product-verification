package org.firas.common.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;

/**
 * 用于访问一个对象中某个属性的工具类
 */
public final class BeanUtils {

    private BeanUtils() {}

    /**
     * 获取一个属性字段的getter方法
     * @param field 属性字段
     * @return 该属性字段的getter方法
     */
    public static Method getterFor(Field field) {
        String suffix = firstCapital(field.getName());
        Class<?> clazz = field.getDeclaringClass();
        Class<?> fieldClass = field.getType();
        try {
            Method method = clazz.getDeclaredMethod("get" + suffix);
            if (method.getReturnType().isAssignableFrom(fieldClass)) {
                return method;
            }
        } catch (NoSuchMethodException ex) {}
        if (!fieldClass.equals(boolean.class) && !fieldClass.equals(Boolean.class)) {
            return null;
        }
        try {
            Method method = clazz.getDeclaredMethod("is" + suffix);
            if (method.getReturnType().isAssignableFrom(fieldClass)) {
                return method;
            }
        } catch (NoSuchMethodException ex) {}
        return null;
    }

    /**
     * 获取一个对象的指定属性的值
     * @param field 属性字段
     * @param bean 要获取属性的对象
     * @return 该对象该属性的值
     */
    public static Object getField(Field field, Object bean)
            throws InvocationTargetException, IllegalAccessException {
        Method getter = getterFor(field);
        if (null != getter) {
            return getter.invoke(bean);
        }
        field.setAccessible(true);
        return field.get(bean);
    }

    /**
     * 获取一个对象的指定属性的值
     * @param fieldName 属性的名称
     * @param bean 要获取属性的对象
     * @return 该对象该属性的值
     */
    public static Object getField(String fieldName, Object bean)
            throws InvocationTargetException, IllegalAccessException,
            NoSuchFieldException {
        for (Class clazz = bean.getClass();
                null != clazz && !Object.class.equals(clazz);
                clazz = clazz.getSuperclass()) {
            try {
                return getField(clazz.getDeclaredField(fieldName), bean);
            } catch (NoSuchFieldException ex) { }
        }
        throw new NoSuchFieldException("There is no field named [" +
                fieldName + "] in " + bean.getClass().getName());
    }

    /**
     * 获取一个属性字段的setter方法
     * @param field 属性字段
     * @return 该属性字段的setter方法
     */
    public static Method setterFor(Field field) {
        String suffix = firstCapital(field.getName());
        String setterName = "set" + suffix;
        Class<?> clazz = field.getDeclaringClass();
        Class<?> fieldClass = field.getType();
        try {
            return clazz.getDeclaredMethod(setterName, fieldClass);
        } catch (NoSuchMethodException ex) {}
        for (Method method : clazz.getDeclaredMethods()) {
            if (method.getName().equals(setterName)) {
                Class[] parameterTypes = method.getParameterTypes();
                if (1 == parameterTypes.length &&
                        fieldClass.isAssignableFrom(parameterTypes[0])) {
                    return method;
                }
            }
        }
        return null;
    }

    /**
     * 设置一个对象的指定属性的值
     * @param field 属性字段
     * @param bean 要获取属性的对象
     * @param value 属性的值
     */
    public static void setField(Field field, Object bean, Object value)
            throws InvocationTargetException, IllegalAccessException {
        Method setter = setterFor(field);
        if (null != setter) {
            setter.invoke(bean, value);
        }
        field.setAccessible(true);
        field.set(bean, value);
    }

    /**
     * 设置一个对象的指定属性的值
     * @param fieldName 属性的名称
     * @param bean 要获取属性的对象
     * @param value 属性的值
     */
    public static void setField(String fieldName, Object bean, Object value)
            throws InvocationTargetException, IllegalAccessException,
            NoSuchFieldException {
        for (Class clazz = bean.getClass();
             null != clazz && !Object.class.equals(clazz);
             clazz = clazz.getSuperclass()) {
            try {
                setField(clazz.getDeclaredField(fieldName), bean, value);
                return;
            } catch (NoSuchFieldException ex) { }
        }
        throw new NoSuchFieldException("There is no field named [" +
                fieldName + "] in " + bean.getClass().getName());
    }

    public interface FieldSetter<T> {
        Class<T> getType();
        <A extends Annotation> A getAnnotation(Class<A> clazz);
        void setField(Object obj, T value) throws Exception;
    }

    /**
     * 获取某个对象在某个属性上的指定注解
     * @param bean 对象
     * @param fieldName 属性名
     * @param annotationClass 注解的类
     * @param <T> 注解的类型
     * @return 该对象在该属性上的指定注解
     * @throws NoSuchFieldException 该对象中没有指定属性
     */
    public static <T extends Annotation> T getAnnotationForField(Object bean,
            String fieldName, Class<T> annotationClass) throws NoSuchFieldException {
        return getAnnotationForField(bean.getClass(), fieldName, annotationClass);
    }

    /**
     * 获取某个类在某个属性上的指定注解
     * @param clazz 类
     * @param fieldName 属性名
     * @param annotationClass 注解的类
     * @param <T> 注解的类型
     * @return 该类在该属性上的指定注解
     * @throws NoSuchFieldException 该类中没有指定属性
     */
    public static <T extends Annotation> T getAnnotationForField(Class<?> clazz,
            String fieldName, Class<T> annotationClass) throws NoSuchFieldException {
        Field field = null;
        while (null != clazz && !Object.class.equals(clazz)) {
            try {
                field = clazz.getDeclaredField(fieldName);
                break;
            } catch (NoSuchFieldException ex) {}
            clazz = clazz.getSuperclass();
        }
        if (null == field) {
            throw new NoSuchFieldException("The bean does NOT has a field named \"" +
                    fieldName + "\"");
        }
        return field.getAnnotation(annotationClass);
    }

    /**
     * 用于获取某个属性的getter/setter的名称
     * @param str 一个属性名
     * @return 首字母大写的属性名
     */
    private static String firstCapital(String str) {
        return str.substring(0, 1).toUpperCase(Locale.US) + str.substring(1);
    }
}
