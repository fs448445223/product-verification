package org.firas.common.util;

import java.time.DateTimeException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.YearMonth;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * <b><code></code></b>
 * <p/>
 * 使用 Java 8 的线程安全的新日期时间 API 的日期时间工具类
 * <p/>
 * <b>Creation Time:</b> 2018年8月22日
 *
 * @author Wu Yuping
 * @version 1.0.0
 * @since 1.0.0
 */
public final class Java8DateUtils {

    // 工具类不允许实例化
    private Java8DateUtils() {}

    /**
     * 判断某一年是否闰年
     * @param year 年份
     * @return year是否闰年
     */
    public static boolean isLenient(short year) {
        return year % (year % 100 == 0 ? 400 : 4) == 0;
    }

    public static OffsetDateTime nowToDateTime() {
        return OffsetDateTime.now(myZoneOffset);
    }

    public static OffsetDateTime millisToDateTime(final long millis) {
        return OffsetDateTime.ofInstant(
                Instant.ofEpochMilli(millis), myZoneOffset);
    }

    public static OffsetDateTime dateToDateTime(final Date date) {
        return millisToDateTime(date.getTime());
    }

    public static OffsetDateTime calendarToDateTime(final Calendar calendar) {
        return millisToDateTime(calendar.getTimeInMillis());
    }

    public static OffsetDateTime parseToDateTime(final String str,
            final DateTimeFormatter formatter) throws DateTimeParseException {
        try {
            return OffsetDateTime.parse(str, formatter)
                    .withOffsetSameInstant(myZoneOffset);
        } catch (DateTimeParseException ex) {
            if (cannotObtainZone(ex)) {
                try {
                    return LocalDateTime.parse(str, formatter).atOffset(myZoneOffset);
                } catch (DateTimeParseException ex1) {
                    if (cannotObtainTime(ex1)) {
                        return LocalDate.parse(str, formatter)
                                .atTime(0, 0, 0, 0)
                                .atOffset(myZoneOffset);
                    }
                    throw ex1;
                }
            }
            throw ex;
        }
    }

    public static Date parseToDate(final String str,
            final DateTimeFormatter formatter) throws DateTimeParseException {
        Date result = new Date();
        result.setTime(parseToDateTime(str, formatter).toInstant().toEpochMilli());
        return result;
    }

    public static Calendar parseToCalendar(final String str,
            final DateTimeFormatter formatter) throws DateTimeParseException {
        Calendar result = Calendar.getInstance(myTimeZone);
        result.setTimeInMillis(parseToDateTime(str, formatter)
                .toInstant().toEpochMilli());
        return result;
    }

    public static OffsetDateTime dayBegin(OffsetDateTime dateTime) {
        return dateTime.withHour(0).withMinute(0).withSecond(0).withNano(0);
    }

    public static OffsetDateTime dayEnd(OffsetDateTime dateTime) {
        return dateTime.withHour(23).withMinute(59).withSecond(59).withNano(999999999);
    }

    public static OffsetDateTime monthBegin(OffsetDateTime dateTime) {
        return dayBegin(dateTime.withDayOfMonth(1));
    }

    public static OffsetDateTime monthEnd(OffsetDateTime dateTime) {
        YearMonth yearMonth = YearMonth.from(dateTime);
        return dayEnd(dateTime.withDayOfMonth(yearMonth.lengthOfMonth()));
    }

    public static OffsetDateTime yearBegin(OffsetDateTime dateTime) {
        return monthBegin(dateTime.withMonth(1));
    }

    public static OffsetDateTime yearEnd(OffsetDateTime dateTime) {
        return monthEnd(dateTime.withMonth(12));
    }

    public static LocalDateTime dayBegin(LocalDateTime dateTime) {
        return dateTime.withHour(0).withMinute(0).withSecond(0).withNano(0);
    }

    public static LocalDateTime dayEnd(LocalDateTime dateTime) {
        return dateTime.withHour(23).withMinute(59).withSecond(59).withNano(999999999);
    }

    public static LocalDateTime monthBegin(LocalDateTime dateTime) {
        return dayBegin(dateTime.withDayOfMonth(1));
    }

    public static LocalDateTime monthEnd(LocalDateTime dateTime) {
        YearMonth yearMonth = YearMonth.from(dateTime);
        return dayEnd(dateTime.withDayOfMonth(yearMonth.lengthOfMonth()));
    }

    public static LocalDateTime yearBegin(LocalDateTime dateTime) {
        return monthBegin(dateTime.withMonth(1));
    }

    public static LocalDateTime yearEnd(LocalDateTime dateTime) {
        return monthEnd(dateTime.withMonth(12));
    }

    private static boolean cannotObtainZone(
            final DateTimeParseException ex) {
        if (!(ex.getCause() instanceof DateTimeException)) {
            return false;
        }
        final DateTimeException cause =
                DateTimeException.class.cast(ex.getCause());
        if (!(cause.getCause() instanceof DateTimeException)) {
            return false;
        }
        return DateTimeException.class.cast(cause.getCause()).getMessage()
                    .contains("Unable to obtain ZoneOffset from");
    }

    private static boolean cannotObtainTime(
            final DateTimeParseException ex) {
        if (!(ex.getCause() instanceof DateTimeException)) {
            return false;
        }
        final DateTimeException cause =
                DateTimeException.class.cast(ex.getCause());
        if (!(cause.getCause() instanceof DateTimeException)) {
            return false;
        }
        return DateTimeException.class.cast(cause.getCause()).getMessage()
                .contains("Unable to obtain LocalTime from");
    }

    public static final DateTimeFormatter yyyyMMddFormatter =
            DateTimeFormatter.ofPattern("yyyyMMdd");

    public static final DateTimeFormatter yyyyMMddHHmmssFormatter =
            DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

    public static final DateTimeFormatter yyyy_MM_ddFormatter =
            DateTimeFormatter.ISO_LOCAL_DATE;

    public static final DateTimeFormatter yyyy_MM_dd_HH_mm_ssFormatter =
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    // 北京时间
    public static final ZoneOffset myZoneOffset = ZoneOffset.ofHours(8);
    public static final TimeZone myTimeZone =
            TimeZone.getTimeZone(myZoneOffset.normalized());
}
