package org.firas.common.util;

import java.util.UUID;

/**
 * <b><code></code></b>
 * <p/>
 * UUID（无横线“-”）生成器
 * <p/>
 * <b>Creation Time:</b> 2018年8月22日
 * @author Wu Yuping
 * @version 1.0.0
 * @since 1.0.0
 */
public final class UuidGenerator {

    private UuidGenerator() {}

    public static String getUuid() {
        UUID uuid = UUID.randomUUID();
        String[] temp = new String[] {
                Long.toHexString(uuid.getMostSignificantBits()),
                Long.toHexString(uuid.getLeastSignificantBits())
        };
        return allZeros.substring(temp[0].length()) + temp[0] +
                allZeros.substring(temp[1].length()) + temp[1];
    }

    private static final String allZeros = "0000000000000000";
}
