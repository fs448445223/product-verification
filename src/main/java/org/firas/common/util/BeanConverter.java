package org.firas.common.util;

import org.firas.common.exception.ConversionException;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/**
 * <b><code></code></b>
 * <p/>
 * 将一个类的对象转换为另一个类的对象
 * 新对象的类中与旧对象的类的字段同名的，
 * 本工具会尝试将旧对象的该字段的值赋值给新对象的对应字段
 * <p/>
 * <b>Creation Time:</b> 2018-09-10
 * @author Wu Yuping
 * @version 1.1.0
 * @since 1.0.0
 */
public class BeanConverter<SRC, TARGET> {

    public BeanConverter(Class<TARGET> targetClass) {
        if (null == targetClass) {
            throw new NullPointerException("targetClass cannot be null");
        }
        this.targetClass = targetClass;
    }

    public TARGET convert(SRC src) throws ConversionException {
        if (null == src) {
            return null;
        }
        TARGET result;
        try {
            result = targetClass.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new ConversionException("转换出错：创建新类的对象出错", ex);
        }
        for (Class clazz = src.getClass();
                null != clazz && !Object.class.equals(clazz);
                clazz = clazz.getSuperclass()) {
            for (Field field : clazz.getDeclaredFields()) {
                try {
                    BeanUtils.setField(field.getName(), result,
                            BeanUtils.getField(field, src));
                } catch (InvocationTargetException ex) {
                } catch (IllegalAccessException ex) {
                } catch (NoSuchFieldException ex) {
                }
            }
        }
        return result;
    }

    private Class<TARGET> targetClass;
}
