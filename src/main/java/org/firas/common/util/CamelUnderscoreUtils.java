package org.firas.common.util;

/**
 * <b><code></code></b>
 * <p/>
 * <p>
 * <p/>
 *
 * <b>Creation Time:</b> 2018年09月14日
 *
 * @author Wu Yuping
 * @version 1.0.0
 * @since 1.0.0
 */
public final class CamelUnderscoreUtils {

    private CamelUnderscoreUtils() {}

    public static String toCamelCase(String name) {
        StringBuilder builder = new StringBuilder();
        boolean afterUnderscore = false;
        for (int i = 0; i < name.length(); i += 1) {
            char c = name.charAt(i);
            if ('_' == c) {
                afterUnderscore = true;
            } else if (afterUnderscore) {
                builder.append(Character.toUpperCase(c));
                afterUnderscore = false;
            } else {
                builder.append(Character.toLowerCase(c));
            }
        }
        return builder.toString();
    }

    public static String toUnderscore(String name) {
        StringBuilder builder = new StringBuilder();
        boolean afterUpperCase = false;
        for (int i = 0; i < name.length(); i += 1) {
            char c = name.charAt(i);
            if (Character.isUpperCase(c)) {
                builder.append('_').append(Character.toLowerCase(c));
            } else {
                builder.append(c);
            }
        }
        return builder.toString();
    }
}
