package org.firas.common.util;

import java.lang.reflect.Field;
import java.util.HashMap;

/**
 * <b><code></code></b>
 * <p/>
 * 用来确定最优的HashSet或HashMap的初始大小
 * 以避免HashSet或HashMap的扩容并重新哈希
 * <p/>
 *
 * <b>Creation Time:</b> 2018年08月28日
 *
 * @author Wu Yuping
 * @version 1.0.0
 * @since 1.0.0
 */
public final class HashSizeUtils {

    private HashSizeUtils() {}

    public static int capacityFor(int size, float loadFactor) {
        return Math.max((int)Math.ceil(size / loadFactor), 8);
    }

    public static int capacityFor(int size) {
        return capacityFor(size, DEFAULT_LOAD_FACTOR);
    }

    private static float DEFAULT_LOAD_FACTOR;
    static {
        try {
            Field field = HashMap.class.getDeclaredField("DEFAULT_LOAD_FACTOR");
            DEFAULT_LOAD_FACTOR = field.getFloat(null);
        } catch (NoSuchFieldException | IllegalAccessException ex) {
            DEFAULT_LOAD_FACTOR = 0.75f;
        }
    }
}
