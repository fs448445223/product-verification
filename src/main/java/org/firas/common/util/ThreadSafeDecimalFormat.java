package org.firas.common.util;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * <b><code></code></b>
 * <p/>
 * <p>
 * <p/>
 *
 * <b>Creation Time:</b> 2018年09月13日
 *
 * @author Wu Yuping
 * @version 1.0.0
 * @since 1.0.0
 */
public final class ThreadSafeDecimalFormat {

    private ThreadSafeDecimalFormat() {}

    public static DecimalFormat getDecimalFormat(final String pattern,
            final RoundingMode roundingMode) {
        return threadLocal.get().computeIfAbsent(roundingMode.name() + '^' + pattern, (key) -> {
            DecimalFormat result = new DecimalFormat(pattern);
            result.setRoundingMode(roundingMode);
            return result;
        });
    }

    private static final ThreadLocal<Map<String, DecimalFormat>> threadLocal =
            ThreadLocal.withInitial(HashMap::new);
}
