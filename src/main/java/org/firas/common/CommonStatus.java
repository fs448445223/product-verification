package org.firas.common;

import lombok.Getter;

public enum CommonStatus {

    DELETED(0, "已删除"),
    NORMAL(1, "正常"),
    FROZEN(2, "已冻结"),
    AUDITING(3, "审核中"),
    EXPIRED(4, "已过期");

    CommonStatus(final int code, final String desc) {
        this.code = code;
        this.desc = desc;
    }

    @Getter private final int code;
    @Getter private final String desc;
}
