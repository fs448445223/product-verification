package org.firas.common.exception;

public class EntityNotExistsException extends Exception {

    public EntityNotExistsException(String message) {
        super(message);
    }

    public EntityNotExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
