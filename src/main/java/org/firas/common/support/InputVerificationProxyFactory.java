package org.firas.common.support;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.executable.ExecutableValidator;

import org.firas.common.lang.GenericType;
import org.springframework.beans.factory.FactoryBean;

/**
 * <b><code></code></b>
 * <p/>
 * <p>
 * <p/>
 *
 * <b>Creation Time:</b> 2018年09月28日
 *
 * @author Wu Yuping
 * @version 1.0.0
 * @since 1.0.0
 */
public class InputVerificationProxyFactory<T> implements FactoryBean<T> {

    @Override
    @SuppressWarnings("unchecked")
    public T getObject() {
        if (null == target) {
            throw new IllegalStateException("target not set");
        }
        return (T) Proxy.newProxyInstance(getClass().getClassLoader(), getInterfaces(),
                (proxy, method, args) -> {
            final ExecutableValidator validator = Validation
                    .buildDefaultValidatorFactory()
                    .getValidator().forExecutables();
            Set<ConstraintViolation<T>> violations =
                    validator.validateParameters(target, method, args);
            if (null != violations && !violations.isEmpty()) {
                throw new IllegalArgumentException(
                        violations.iterator().next().getMessage());
            }
            return method.invoke(target, args);
        });
    }

    private T target;
    public void setTarget(T target) {
        this.target = target;
    }
    private Class<?>[] getInterfaces() {
        return target.getClass().getInterfaces();
    }

    private GenericType<T> genericType = new GenericType<T>() {
        @Override
        public Type getType() {
            Type type = super.getType();
            if (type instanceof Class) {
                return type;
            }
            if (type instanceof ParameterizedType) {
                return ParameterizedType.class.cast(type).getRawType();
            }
            if (null != target) {
                Class[] interfaces = target.getClass().getInterfaces();
                if (interfaces.length > 0) {
                    return interfaces[0];
                }
            }
            return null;
        }
    };
    @Override
    public Class<?> getObjectType() {
        return (Class<?>) genericType.getType();
    }

    private boolean singleton = true;
    @Override
    public boolean isSingleton() {
        return singleton;
    }
    public void setSingleton(boolean singleton) {
        this.singleton = singleton;
    }
}
