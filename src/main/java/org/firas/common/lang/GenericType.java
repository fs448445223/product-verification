package org.firas.common.lang;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * <b><code></code></b>
 * <p/>
 * 用于获取泛型类型的工具类
 * <p/>
 * 用法示例：(new GenericType&lt;HashMap&lt;String, Object&gt;&gt;() {}).getType()
 *           // 返回 ParameterizedType (HashMap&lt;String, Object&gt;)
 * <p/>
 * <b>Creation Time:</b> 2018年8月22日
 * @param <T> 泛型类型
 *
 * @author Wu Yuping
 * @version 1.0.0
 * @since 1.0.0
 */
public abstract class GenericType<T> implements TypeForGeneric<GenericType<T>> {

    public GenericType() {
        Type superType = this.getClass().getGenericSuperclass();
        this.type = ParameterizedType.class.cast(superType).getActualTypeArguments()[0];
    }

    public Type getType() {
        return type;
    }

    private Type type;
}
