package org.firas.common.lang;

import java.lang.reflect.Type;

/**
 * <b><code></code></b>
 * <p/>
 * 仅用于获取泛型类型的工具类
 * <p/>
 * <b>Creation Time:</b> 2018年8月22日
 * @param <T> 用于获取泛型类型的工具类
 *
 * @author Wu Yuping
 * @version 1.0.0
 * @since 1.0.0
 */
interface TypeForGeneric<T> {

    Type getType();
}
