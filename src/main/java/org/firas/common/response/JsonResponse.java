package org.firas.common.response;

import lombok.Getter;

import java.io.Serializable;

public class JsonResponse<T extends Serializable> implements Serializable {

    public JsonResponse(final String code, final String message, final T content) {
        this(code, message, message, content);
    }

    public JsonResponse(final String code, final String message,
            final String desc, final T content) {
        this.code = code;
        this.message = message;
        this.desc = desc;
        this.content = content;
        this.timestamp = System.currentTimeMillis();
    }

    @Getter private String code;
    @Getter private String message;
    @Getter private String desc;
    @Getter private T content;
    @Getter private long timestamp;
}
