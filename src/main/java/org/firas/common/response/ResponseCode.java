package org.firas.common.response;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ResponseCode {
    SUCCESS("0000", "成功", "成功"),
    NEED_LOGIN("A001", "需要登录/重新登录", "请登录"),
    TOO_FREQUENT("F001", "访问过于频繁", "网站忙不过来了");
    @Getter
    private final String code, desc, message;
}
