package org.firas.verif.po;

import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.*;

public class CompanyPO implements Serializable {

    @Getter @Setter
    private int id;

    @Getter @Setter
    private String name;

    @Getter @Setter
    private String status;

    @Getter @Setter
    private LocalDateTime createTime;

    @Getter @Setter
    private LocalDateTime updateTime;

    @Getter @Setter
    private String createUser;

    @Getter @Setter
    private String updateUser;
}