package org.firas.verif.po;

import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

public class ProductPO implements Serializable {

    @Getter @Setter
    private String id;

    @Getter @Setter
    private CompanyPO company;

    @Getter @Setter
    private String name;

    @Getter @Setter
    private LocalDateTime createTime;

    @Getter @Setter
    private LocalDateTime updateTime;

    @Getter @Setter
    private String createUser;

    @Getter @Setter
    private String updateUser;
}
