package org.firas.verif.service;

import org.firas.verif.bo.Product;
import org.firas.common.exception.EntityNotExistsException;

import java.util.List;

public interface ProductManager {

    Product getProductById(String productId) throws EntityNotExistsException;

    List<Product> createProductBatch(String companyId, String productName,
            String number) throws EntityNotExistsException;
}
