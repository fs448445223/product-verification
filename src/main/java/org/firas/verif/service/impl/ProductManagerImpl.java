package org.firas.verif.service.impl;

import org.firas.common.exception.ConversionException;
import org.firas.common.util.BeanConverter;
import org.firas.common.util.UuidGenerator;
import org.firas.verif.bo.Product;
import org.firas.verif.dao.ProductDao;
import org.firas.verif.po.CompanyPO;
import org.firas.verif.po.ProductPO;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProductManagerImpl {

    public List<Product> createProductBatch(
            @Positive(message = "{org.firas.verif.service.ProductManager.createProductBatch.companyId.message")
                    @Valid int companyId,
            String name,
            @Positive(message = "{org.firas.verif.service.ProductManager.createProductBatch.number.message")
            @DecimalMax(value = "1000",
                    message = "{org.firas.verif.service.ProductManager.createProductBatch.number.message")
                    @Valid int number) {
        final LocalDateTime now = LocalDateTime.now();
        List<ProductPO> toSave = new ArrayList<>(number);
        for (int i = number; i > 0; i -= 1) {
            CompanyPO companyPO = new CompanyPO();
            companyPO.setId(companyId);
            ProductPO po = new ProductPO();
            po.setCompany(companyPO);
            po.setCreateTime(now);
            po.setId(UuidGenerator.getUuid());
            po.setName(name);
            toSave.add(po);
        }
        return productDao.saveAll(toSave).parallelStream().map((po) -> {
            try {
                return toBoConverter.convert(po);
            } catch (ConversionException ex) {
                throw new RuntimeException("转换出错", ex);
            }
        }).collect(Collectors.toList());
    }

    public void setProductDao(ProductDao productDao) {
        this.productDao = productDao;
    }

    private ProductDao productDao;

    private static final BeanConverter<ProductPO, Product> toBoConverter =
            new BeanConverter<>(Product.class);
    private static final BeanConverter<Product, ProductPO> fromBoConverter =
            new BeanConverter<>(ProductPO.class);
}
