package org.firas.verif.service.impl;

import org.firas.common.exception.ConversionException;
import org.firas.common.exception.EntityNotExistsException;
import org.firas.common.util.BeanConverter;
import org.firas.verif.bo.Company;
import org.firas.verif.dao.CompanyDao;
import org.firas.verif.po.CompanyPO;
import org.firas.verif.service.CompanyManager;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class CompanyManagerImpl implements CompanyManager {

    @Override
    public Company getById(String id) throws Exception {
        try {
            return toBoConverter.convert(companyDao.findById(
                    Integer.parseInt(id)).orElseThrow(
                            () -> new EntityNotExistsException("系统中找不到该ID的公司")));
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("ID必须是一个正整数");
        }
    }

    @Override
    public List<Company> search() {
        return companyDao.findAll().stream().map((po) -> {
            try {
                return toBoConverter.convert(po);
            } catch (ConversionException ex) {
                throw new RuntimeException("转换出错", ex);
            }
        }).collect(Collectors.toList());
    }

    @Override
    public void create(Company company) throws Exception {
        CompanyPO po = fromBoConverter.convert(company);
        if (null == po.getCreateTime()) {
            po.setCreateTime(LocalDateTime.now());
        }
        if (null == po.getCreateUser()) {
            // TODO
        }
        po = companyDao.save(po);
        company.setId(po.getId());
    }

    @Override
    public void updateById(Company company) throws Exception {
        if (company.getId() <= 0) {
            throw new IllegalArgumentException("ID必须是一个正整数");
        }
        CompanyPO po = fromBoConverter.convert(company);
        if (null == po.getUpdateTime()) {
            po.setUpdateTime(LocalDateTime.now());
        }
        if (null == po.getUpdateUser()) {
            // TODO
        }
        companyDao.save(po);
    }

    public void setCompanyDao(CompanyDao companyDao) {
        this.companyDao = companyDao;
    }

    private CompanyDao companyDao;

    private static final BeanConverter<CompanyPO, Company> toBoConverter =
            new BeanConverter<>(Company.class);
    private static final BeanConverter<Company, CompanyPO> fromBoConverter =
            new BeanConverter<>(CompanyPO.class);
}
