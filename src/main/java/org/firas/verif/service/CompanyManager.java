package org.firas.verif.service;

import org.firas.verif.bo.Company;

import java.util.List;

public interface CompanyManager {

    Company getById(String id) throws Exception;
    List<Company> search() throws Exception;
    void create(Company company) throws Exception;
    void updateById(Company company) throws Exception;
}
