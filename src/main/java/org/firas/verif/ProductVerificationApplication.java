package org.firas.verif;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("context-root.xml")
public class ProductVerificationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductVerificationApplication.class, args);
	}
}
