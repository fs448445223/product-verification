package org.firas.verif.controller;

import org.firas.common.response.JsonResponse;
import org.firas.common.response.ResponseCode;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("product")
public class ProductController {

    @PostMapping()
    public JsonResponse getProductIds(final String companyId,
            final String productName, final String number) {
        return new JsonResponse(ResponseCode.SUCCESS.getCode(),
                "", null);
    }
}
