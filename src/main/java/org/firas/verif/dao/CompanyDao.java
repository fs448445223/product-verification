package org.firas.verif.dao;

import org.firas.common.dao.DaoBase;
import org.firas.verif.po.CompanyPO;

public interface CompanyDao extends DaoBase<CompanyPO, Integer> {
}
