package org.firas.verif.dao;

import org.firas.common.dao.DaoBase;
import org.firas.verif.po.ProductPO;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductDao extends DaoBase<ProductPO, Long> {
}
