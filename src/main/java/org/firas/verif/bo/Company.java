package org.firas.verif.bo;

import java.io.Serializable;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class Company implements Serializable {

    @Getter @Setter
    @PositiveOrZero(message = "{org.firas.verif.bo.Company.id.message}")
    private int id = 0;

    @Getter @Setter
    @NotNull(message = "{org.firas.verif.bo.Company.name.message}")
    @Size(min = 1, max = 100, message = "{org.firas.verif.bo.Company.name.message}")
    private String name;

    @Getter @Setter
    @NotNull(message = "{org.firas.verif.bo.Company.status.message}")
    @Pattern(regexp = "[01]", message = "{org.firas.verif.bo.Company.status.message}")
    private String status;
}