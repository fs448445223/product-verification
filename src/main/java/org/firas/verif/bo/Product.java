package org.firas.verif.bo;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Product implements Serializable {

    @Getter @Setter
    @Pattern(regexp = "[0-9a-f]{32}", message = "{org.firas.verif.bo.Product.id.message}")
    private String id;

    @Getter @Setter
    @NotNull(message = "{org.firas.verif.bo.Company.name.message}")
    @Size(min = 1, max = 100, message = "{org.firas.verif.bo.Company.name.message}")
    private String name;

    @Getter @Setter private Company company;

    @Getter @Setter private LocalDateTime createTime;
}
